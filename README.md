# Interview Coding Challenges #
You may use your choice of development tools, package manager (NPM or Bower), or 3rd-party libraries to solve the challenges.
You are allowed to perform quick Google searches throughout the challenge. If you have any questions regarding the challenges, feel free to ask me.

## Challenges ##
1. Node.js / Sails.js Database
1. Writing SQL
1. AngularJS

### Node.js / [Sails.js](http://sailsjs.com/) ###
Goal: Using Sails.js, create a database that tracks cars and people. You are not required to RUN Sails.js but you must use Sails.js syntax to create it.

#### Properties ####
Cars - make, model, color, year, year bought

People - first name, last name, ability to own cars

### PostgreSQL ###
Using a method of your choice, write 2 SQL statements:

1. All cars bought after the year they were made. Ex. A Honda 2008 that was bought in 2010
1. All people who bought a car after 2016

Check the SQL validity [here](https://www.piliapp.com/mysql-syntax-check/).


### AngularJS ###
Goal: Create page that tracks car inventory using AngularJS components. If able to connect to database, do so. If not, no sweat use provided hard coded array as data source.

1. Display all cars in the inventory using a tile layout - doesn't have to be perfect
1. Ability to filter results by car year. Can be a dropdown, checkbox, radio, text, etc

~~~~
[
	{
		make: "Honda",
		model: "Accord",
		color: "Red",
		year: 2008,
		yearBought: 2017
	},
	{
		make: "Honda",
		model: "Civic",
		color: "Blue",
		year: 2004,
		yearBought: 2004
	},
	{
		make: "Toyota",
		model: "Camry",
		color: "Black",
		year: 2017,
		yearBought: 2017
	},
	{
		make: "Nissan",
		model: "Altima",
		color: "White",
		year: 2013,
		yearBought: 2014
	},
	{
		make: "Audi",
		model: "A3",
		color: "Red",
		year: 2014,
		yearBought: 2014
	},
	{
		make: "Honda",
		model: "Accord",
		color: "Black",
		year: 2001,
		yearBought: 2001
	},
	{
		make: "Toyota",
		model: "Prius",
		color: "Black",
		year: 2008,
		yearBought: 2009
	},
	{
		make: "Toyota",
		model: "Camry",
		color: "Yellow",
		year: 2013,
		yearBought: 2017
	}
]
~~~~